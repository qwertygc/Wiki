# Version 20250224
Correction d'un bug permettant de renommer le slug qui est vide.

# Version 20241202
Ajout système de membre

# Version 20230805

Création dans la configuration d‘une « page d’accueil » par défaut.

# Version 20230307

Correction bug historique permettant de supprimer des pages sans être connecter

# Version 20220721
Fix erreur $_GET['r']

# Version 20220103
Redirection sur la page quand on se connecte
Ajout tableau

# Version 20211231
Refonte intégrale du Wiki pour passer d'un système de stockage basé sur le plain text à un système basé sur le stockage en base SQL. Il y a surement plein de bugs.

Changement dans les dépendances, en se basant sur CommonMark pour la génération de Markdown et jfcherng/php-diff pour la gestion de différence des versions. Utilisation de Gettext pour la traduction.

Quelques améliorations graphiques dans le CSS, avec notamment un peaufinage des formulaires et la création d'un thème « sombre ».

Passage à la licence CeCILL à la licence EUPL. 

Pour faire la migration : 1) faire une sauvegarde du dossier data et le renommer en data_files. 2) Installer le wiki dans une instance « neuve ». 3) aller dans api.php?do=files2sql et faire la migration.


# Version 20210628

## Added

## Changed

## Deprecated

## Removed

## Fixed
- fix bug quand edit page

## Security

# Version 20210626

## Added

## Changed
- Les dossiers des contenus texte sont déplacés de data/ vers data/pages

## Deprecated

## Removed

## Fixed

## Security

# Version 20201008

## Added


## Changed
- Unicornsystem en composer
## Deprecated

## Removed

## Fixed


# Version 20200802

## Added


## Changed
- Allègement thème Ouccitanio
## Deprecated

## Removed

## Fixed

- Corrige exportation d'une page

# Version 20200801

## Added

- Ajout d'un bouton « exporter » sur une page du wiki et dans la configuration du wiki (export général)
- Création d'un système de sauvegarde

## Changed

- Modification des métadonnées YAML dans l'exportation pour qu'elle soit conforme à ce qui se fait dans Zettlr et pandoc
- Modification dans la manière de mettre à jour

## Deprecated

## Removed

## Fixed


## Security

# Version 20200723

## Added

## Changed

- Mise à jour des librairies

## Deprecated

## Removed

## Fixed
- Fix le message d'erreur quand on accède à une page qui n'existe pas

## Security

# Version 20200523
## Added
## Changed
- Comparaison historique : comparaison au caractère et plus au mot
- Modification de la dépendance FineDiff : maintenant prise en compte des caractères non ascii
## Deprecated
## Removed
## Fixed
## Security
# Version 20200516

## Added

## Changed

- Parsedown TOC est maintenant mis en composer

## Deprecated

## Removed

## Fixed

## Security

# Version 20200510

## Added

## Changed

- Réorganisation interne des librairies

## Deprecated

## Removed

## Fixed

## Security

# Version 20200503

## Added
- Création d'une page d'installation
## Changed
- Passage à composer
- Utilisation des forks de TOTPHP et PHPDIFF
## Deprecated

## Removed

## Fixed

## Security

# Version 20200428

## Added
- Possibilité de supprimer des fichiers téléversés
- Possibilité de renommer une page

## Changed
- Array pour les fichiers exclus du Scan
- Donne code Markdown pour les images dans la liste des médias
- Restructuration liste des fichiers téléversés
- Renommage de l'option Katex en scientific.
- Ajout icone lien externe dans le CSS

## Deprecated

## Removed

## Fixed

## Security


# Version 20200418

## Added

- Création d'un nouveau thème (minimaliste)

## Changed

- Fix le textarea de l'éditeur en mettant en monospace (aidant à voir la mise en page).
- Ajout d'un fieldset pour le champ de connexion


## Deprecated

## Removed

## Fixed

## Security

# Version 20200417

## Added


## Changed
- Modification de l'éditeur pour le rendre plus complet.
- Création de la class « ParsedownUnicorn » pour intégrer les modifications au parsedown (checkbox + mark + subscript et supscript) (fork de Parsedown Extended)

## Deprecated

## Removed

## Fixed

## Security


# Version 20203026

## Added
- Possibilité de mettre un code de suivi statistique (Matamo, G-Analytics etc)
- Possibilité d'ajouter via le gestionnaire de traduction de nouvelles clés

## Changed
- Changement dans l'organisation du fichier langue, sous-array "translation"
- La liste des fichiers langues (avec le nom des langues) se trouvent par fichier, c'est plus un array centralisé qui gère ça.
- Mise à jour de Parsedown, Parsedown Extra et Parsedown TOC
- Le « retenir avec un cookie » est en dessous du formulaire OTP

## Deprecated

## Removed

## Fixed
- Quand on importe une image, on a une URL relative (et l'image est renommé, comme dans l'interface d'upload classique)
- Quand on rentre un code OTP, les espaces sont enlevé automatiquement.
- Fix l'erreur de création du fichier tracker.Txt

## Security

# Version 2003021

## Added
- Possibilité de mettre à jour via l'administration

## Changed

## Deprecated

## Removed

## Fixed

# Version 2003020

## Added

- Prévisualisation des documents dans l'upload

## Changed
- URL relatif lors de l'upload
- Quand c'est une image, donne en formaté markdown


## Deprecated

## Removed

## Fixed

- Fixe erreur dans l'upload

## Security


# Version 20191225

## Added

- Ajout d'un export vers pandoc

- Implantation de [KaTeX](https://katex.org/docs)

- On enlève l'autocomplete des formulaires pour les opérations sensibles

- ajout d'un autocomplete new-password pour générer un nouveau mot de passe

- Système d'authentification avec des cookies

- Ajout d'un lien vers le json pour les langues

## Changed

- ajout des Content-Security-Policy dans le .htaccess

- Tokenisation des opérations sensibles

- Ajoute un max-width pour les images

## Deprecated

## Removed

## Fixed

## Security

# Version 20190911

## Changed
- Date en ISO affiché au survol


# Version 20190910

## Added

- Ajout du thème Oucitanio par Quenti

## Changed
- Optimisation de la feuille de style par défaut

# Version 20190908

## Changed
- Amélioration de l'interface pour la traduction avec la langue source

# Version 20190907

## Fixed

- La checkbox d'urlrewrite dans la configuration n'enregistrait pas la bonne valeur

# Version 20190905

## Added

- Possibilité d'activer ou non l'URL rewrite

## Changed

- Le .htaccess est écrit via PHP

## Fixed

- Quand la chaine de traduction est vide, afficher la clé

# Version 20190904


## Changed

- Changement de deux clés de langues (pour Automatique et langue par défaut)
- Amélioration de la traduction en fr/en.
- Amélioration de la partie applicatif de traduction, rajoutant les éventuelles clés manquantes (depuis le fr)

## Fixed

- Changement de l'arrondi pour TimeAgo() pour qu'il soit plus précis
- Enlève un \ qui traînait


# Version 20190903

## Added
- Encapsule dans une fonction la connexion/déconnexion
- Création d'une fonction qrcode
- Création d'une fonction a() pour les liens
- Ajout d'entrées dans les logs

## Changed

- Changement de l'organisation du fichier config.php
- Mise à jour traduction en occitan (Merci Quent1)
- Modification de l'entête « historique » des articles
- Remplacement Fichiers -> Téléversement dans le menu
- Affichage lien pour visualiser des différentes versions dans le comparateur entre deux versions
- remplacement des redirections de index.php vers la racine
- Détecte le fuseau horaire par défaut et le met lors de l'installation
- Mise en forme du timezone pour que ça soit plus joli dans la configuration
- Tout les boutons submit ont une value maintenant
- Correction des arguments dans les sprintf
- Retour de la date au format ISO 8601 (mais en gardant la date au format humain)
- htmlspecialchars pour le tableau des traductions
- Modification de certaines clés pour la traduction
- Ajout d'un class="empty" pour l'historique
- Les fichiers langues sont en JSON
- Il existe maintenant une interface pour traduire en ligne les chaînes de caractères.

## Fixed

- Correction affichage date comparaison de version
- Correction bug dans le tri des dates des fichiers avec sorttable
- Correction de la fonction TimeAgo pour une meilleure estimation du temps (round() -> floor(), problème d'arrondi)


# Version 20190902

## Added

- Possibilité de choisir une feuille de style personnalisées dans un menu déroulant
- Possibilité de purger l'historique

## Changed

- Formatage des dates de manière lisible pour un humain
- Simplification de la gestion de l'affichage de l'IP


## Fixed
- Correction bug dans le tri des dates/taille des fichiers avec sorttable


# Version 20190901

## Added

- Numéro de version dans la section pour développeurs

# Version 20190830

## Added
- Téléchargement des images par drag and drop dans l'éditeur (un peu buggé, mais c'est au niveau d'EasyMDE).
- Création d'un flux Atom

## Changed
- Organisation des fichiers Parsedown dans Parsedown/
- Fichiers local pour EasyMDE
- Redirection vers siteweb/index au lieu de siteweb/index.php?id=index
- Création d'un fichier regroupant tout les includes
- Création d'une fonction pour l'installation

# Version 20190829

## Changed

- Remplacement de simple markdown editor par easy markdown editor


# Version 20190828


## Added

- Module de recherche
- Possibilité de gérer l'accès à la libre lecture par page

## Changed

- Simplification de certains morceaux de code
- Non-affichage de l'IP si désactivé
- Amélioration de l'affichage de l'OTP dans la configuration
- Amélioration de l'interface d'édition
- Déplace le lien de l'espace développeur dans le footer
- Réorganisation de l'administration
- Dispersion d'éléments de « plans du site »
- Réorganisation des URL


## Removed

- suppression des smileys

## Fixed

- Correction bug compte nombre de modifications


# Version 20190827


## Changed
- Traduction du pied de page


# Version 20190826


## Fixed

- Correction du mode automatique des langues
- Tableaux plus responsive



# Version 20190825

## Added

- Création du fichier data/style.css (pour la personnalisation. Évite de polluer les logs)
-Possibilité de modifier depuis l'administration la feuille de style personnalisée
- Possibilité de cacher l'IP en mode non-connecté
- Création d'une « zone pour développeur ». Pour l'instant, n'affiche que les traductions
- Les scripts inutiles ne sont pas chargés en mode de non-édition
- Ajout d'un pied de page avec le nom du logiciel

## Changed

- Plan du site uniquement accessible si on est connecté
- Liste déroulante pour les fuseaux horaires
- SimpleMDE activé par défaut lors d'une nouvelle installation + désactivation par défaut des smileys sur une nouvelle installation
- La langue du site change selon l'entête du navigateur
- Le QRCode est généré par un script local et plus depuis Google



## Removed

- Suppression de la fonction de compression des pages, buggé et pas utilisé (et pas forcément efficace)
- Suppression de la personnalisation de la couleur de la navbar dans la configuration. Il faut maintenant modifier data/style.css


# Version 20190819

## Added

- Possibilité d'avoir un titre dans les documents
- Changement de nom du logiciel

## Changed

- Déplacement de la notification de mise à jour en haut de la page de configuration.
- Amélioration graphique
- Simple MDE n'est plus dans un CDN mais directement dans l'application


# Version 20190809


## Changed

- Modification et correction de la traduction occitane


# Version 20190802


## Changed

- Possibilité que les smileys ne soient qu'en option


# Version 20190203

## Added

- Modification des URL de Git


# Version 20190202


## Changed

- Ajout une confirmation avant suppression


# Version 20181111

## Fixed

- Case à cocher dans le parseur

	

# Version 20181101

## Added

- ajout du changelog (suivant cette norme https://keepachangelog.com/fr/1.0.0/)

## Changed

- déplacement du code de la table des matières + sa mise en page


# Version 20180923

## Added

- Ajout de l'occitan
- smileys du Zds

## Changed

- modification de l'architecture du wiki. Sortie du monofichier

# Version 20180910

## Added
- Ajout du multi-lingue
- smileys du Zds


# Version 20180717

## Added

- Option pour compresser des pages

## Changed

- factorisation des fonctions pour la lecture/écriture des articles


# Version 20180605

## Removed

- ParseDown Taklist enlevé. Mise en place d'un code perso.


# Version 20180604


## Changed

- Correction d'un bug avec l'éditeur simpleMDE lors de l'installation.
- Pour mettre à  jour les bibliothèques, suffit de supprimer lib/


# Version 20180524

## Added

- ajout de l'éditeur simpleMD


# Version 20180408

## Added

- indication quand une mise à jour est disponible

## Changed

- affichage en KB ou KO
- modification pour lister l'historique


# Version 20180326


## Changed

- sorttable.js en local


# Version 20180220

## Fixed

	- Corrige le problème du GET[id] dans l'URL



# Version 20180218

## Added

- possibilité d'écraser un fichier
- géolocalisation des IP

## Changed

- présentation de l'historique en tableau
- amélioration comparaison historique


# Version 20161226


## Added

- Première release


# Version XXX

## Added

## Changed

## Deprecated

## Removed

## Fixed

## Security
