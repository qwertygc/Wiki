<form action="?do=upload" method="post" enctype="multipart/form-data">
	<fieldset>
		<legend><?= t('Téléverser');?></legend>
		<input type="file" name="file[]" multiple="multiple"/>
		<label for="overwrite"><input type="checkbox" name="overwrite" id="overwrite"/> <?= t('Écraser le(s) fichier(s) existant(s)');?></label>
		<input type="hidden" name="token" id="token" value="<?=$_SESSION['token'];?>"/>
		<input type="submit" name="upload" value="<?=t('Téléverser');?>"/>
	</fieldset>
</form>

<h1><?= t('Fichiers');?></h1>
<table>
	<tr>
		<th><?= t('Nom');?></th>
		<th><?= t('Date');?></th>
		<th><?= t('Format');?></th>
		<th><?= t('Taille');?></th>
		<th><?= t('Supprimer');?></th>
	</tr>				
	<?php foreach (scandir('data/files') as $content): if (!in_array($content, ['.', '..'])): ?>
    <tr>
		<td data-title="<?= t('Nom');?>">
			<a href="data/files/<?=$content;?>" download><?=$content;?></a>
			<input onclick="this.select()" value="<?=mdmedia('data/files/'.$content);?>"/>
			<samp title="sha-1"><?=sha1_file('data/files/'.$content);?></samp>
		</td>
		<td data-title="<?= t('Date');?>"><?=format_date(filemtime('data/files/'.$content));?></td>
		<td data-title="<?= t('Format');?>"><?=mime_content_type('data/files/'.$content);?></td>
		<td data-title="<?= t('Taille');?>"><?=Size('data/files/'.$content)['size'];?></td>
		<td data-title="<?= t('Supprimer');?>"><a href="?do=upload&del=<?=$content;?>&token=<?=$_SESSION['token'];?>">Supprimer</a></td>
	</tr>
	<?php endif; endforeach; ?>
</table>
