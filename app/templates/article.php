<article>
	<div class="block">
		<p><?= t('Dernière modification');?> : <?=format_date($data['time']);?> (<a href="<?=$CONFIG['root'];?>?do=history&id=<?=$data['slug'];?>"><?= t('Historique');?></a>)</p>
		<?php if(checklogin()): ?>
		<p><a href="<?=$CONFIG['root'];?>?do=edit&id=<?=$_GET['id'];?>"><?= t('Modifier');?></a> - <a href="api.php?do=export&id=<?=$_GET['id'];?>"><?= t('Exporter');?></a></p>
		<?php endif; ?>
	</div>
	<header>
		<h1><?=$data['title'];?></h1>
	</header>
	<main><?=$content;?></main>
</article>
